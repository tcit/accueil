#!/bin/bash

# Create working folder
rm -rf ./dist/external/
mkdir -p ./dist/external/img/

# Copy files
cp ./public/img/bg-sky.jpg ././dist/external/img/bg-sky.jpg
cp -R ./commons/assets/fonts ./dist/external/fonts
cp -R ./node_modules/roboto-fontface/fonts/roboto ./dist/external/fonts/roboto

# Build css
sass ./src/scss/frama.scss:./dist/external/css/frama.css

# Cleaning path
sed -i -e 's/..\/..\/public/../g' ./dist/external/css/frama.css
sed -i -e 's/~vue-fs-commons\/src\/assets/../g' ./dist/external/css/frama.css
sed -i -e 's/~roboto-fontface/../g' ./dist/external/css/frama.css


# Build min.css
sass ./src/scss/frama.scss:./dist/external/css/frama.min.css --style compressed

# Cleaning path
sed -i -e 's/..\/..\/public/../g' ./dist/external/css/frama.min.css
sed -i -e 's/~vue-fs-commons\/src\/assets/../g' ./dist/external/css/frama.min.css
sed -i -e 's/~roboto-fontface/../g' ./dist/external/css/frama.min.css

cd dist && zip -r external.zip external