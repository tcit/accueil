title: Manifesto
intro: |-
  ## Preamble

  We – the members of the Framasoft association – will do our best to follow this manifesto;
  we know we are too fallible to promise to follow it word for word!

  Please see this manifesto as a simple declaration of intent of what we want
  to be (and especially do!) in the world.
humans:
  title: We act for a more dignified world, built on sharing, solidarity and the Commons
  desc: |-
    We are revolted by the injustices, indignities and indecencies of
    the current world, shaped by surveillance capitalism.
    We wish to contribute to the emancipation of people through our field of
    action and expertise: the digital and cultural commons.
    We believe that autonomous, proud, knowledgeable, supportive and sharing
    humans can make the world a better place for everyone.
empowerment:
  title: We dream of an emancipating digital world, and do our best to make it happen
  desc: |-
    In a culture where digital technology is omnipresent, we want to offer
    user-friendly and emancipating tools that empower people by respecting
    their integrity and dignity.
    However, we accept our limitations when we try to realise these ideals:
    we do our best, and that’s not bad!
action:
  title: We express ourselves through action and proposal
  desc: |-
    Our way of expression is to do.
    We feel useful in action and believe that our ways of acting reveal our values.
    We see Framasoft as a prefiguration association: we present our achievements
    as prototypes that anyone can appropriate to reproduce them on another scale,
    or adapt them to other constraints.
experimentation:
  title: 'We experiment, even if we fail: in this way, we learn'
  desc: |-
    Having more doubts than certainties, we see what we do as experiments.
    So we accept failure: to err is human, and so are we!
    We even find joy in failures, because they can teach us more than a success.
    But for us, knowledge is more important than efficiency.
commons:
  title: We want to share our productions in the Commons
  desc: |-
    We make sure to share recipes, products and lessons learned freely.
    We elevate our achievements in the Commons.
    We want everyone to be able to take it and adapt it to their own situation,
    but also to avoid being in a position of power because of a monopoly
    on one resource or another.
compostability:
  title: We know that the world will continue to turn after us
  desc: |-
    Neither we nor the association are eternal… and we accept this.
    We act for the compostability of Framasoft, that is to say that we take care
    to leave fertile traces, to create useful ruins so that those who will come
    after us can build with what will remain of our actions.
campfire:
  title: 'We want to take care: of ourselves, of others, and of the Commons that connect us'
  desc: |-
    This statement has its own limitations.
    We have realised that we cannot take care of a Common, or of others, if we
    do not take care of ourselves, any more than we can share a flame
    if we let our own campfire go out.
pathways:
  title: We refuse the power to judge, legitimise or condemn
  desc: |-
    We believe that everyone (including us!) is on a journey.
    We therefore seek to deconstruct the purist reflexes that would make us
    pass moral judgement on the practices and paths of others,
    whether to legitimise or condemn them.
    We also take great care not to value our own modes of action to the detriment
    of those adopted by groups with whom we share values and ideals.
diversity:
  title: We feel complementary to other initiatives
  desc: |-
    We know that we are only one piece of the puzzle.
    What we do is bound to be piecemeal and insufficient.
    This is why we try to maintain solidarity with other collectives, acting
    in other fields, with their own methods.
    In our opinion, the diversity of the modes of action is essential to their impact.
fun:
  title: We don’t take ourselves seriously
  desc: |-
    We don’t want to look any higher than our foundations.
    We don’t hesitate to use jokes to relieve the pressure, even
    if it means offending the serious.
    If humour is at the heart of our practices, it does not detract from
    the substance of our actions.
footer: |-
  Last update of the manifesto: 27 October 2022 (translated by DeepL)